#!/usr/bin/env python
from setuptools import setup

setup(
    name="path-cleaner",
    version="0.0.3",
    author="kmogged",
    description="Command for cleaning up a directory",
    entry_points={"console_scripts": ["path-cleaner=path_cleaner.path_cleaner:main"]},
)
