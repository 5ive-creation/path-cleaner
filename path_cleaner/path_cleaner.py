#!/usr/bin/python3
import argparse
from pathlib import Path
from shutil import rmtree


class Application:
    def __init__(self):
        parser = argparse.ArgumentParser(
            description="command for cleaning up a directory",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        )
        parser.add_argument(
            "--dry-run", "-n", help="do not perform any changes", action="store_true"
        )
        parser.add_argument("path", help="path to be examined and cleaned")
        parser.add_argument(
            "--yes",
            "-y",
            help="do not ask for confirmation before deleting paths",
            action="store_true",
        )
        parser.add_argument(
            "--exclude",
            help="do not delete these paths or symlinks referencing these paths",
            nargs="*",
            default=[],
            type=Path,
        )

        args = parser.parse_args()

        for excluded in args.exclude:
            args.exclude.append(excluded.resolve())
            args.exclude = list(set(args.exclude))

        path = Path(args.path)
        for item in path.glob("*"):
            if item in args.exclude or item.resolve() in args.exclude:
                print("INFO: excluding {}".format(item))
                continue

            if not args.yes:
                answer = input("delete {}? Y/n: ".format(item))
                if not answer == "" and not answer.lower() == "y":
                    continue
            print("WARN: deleting {}".format(item))
            if not args.dry_run:
                if item.is_file():
                    item.unlink()
                if item.is_dir():
                    rmtree(item)

        if args.dry_run:
            print("INFO: dry run, not performing any actions")


def main():
    app = Application()


if __name__ == "__main__":
    main()
